<?php

namespace App\Http\Controllers;
use App\Models\product;

use Illuminate\Http\Request;

class productController extends Controller
{
    //
    public function getallProduct()
    {
        return response()
            ->json(product::get() , 200);
    }
    
    public function order(Request $request)
    {
        $order = product::where('id', '=', $request->get('id'))
            ->first();
            if($order){
                $updateItem=$request->get('available')-1;
                $order->available=  $updateItem;
                $order->save();
            }

        return response()
            ->json($order, 200);
    }
}
